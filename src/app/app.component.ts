import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Kenneth !';
  counter = 0;
  sizeChanged(size){
    console.log("size -> " + size);
    this.counter  = this.counter + 1;
  }
}
